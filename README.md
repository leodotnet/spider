Usage: python3 coinmarketcap.py --volume 1000000 --numcandi 3 --top 10

Options:
  -h, --help           show this help message and exit
  --volume=VOLUME      Volume Above
  --numcandi=NUMCANDI  Number of Candidate coins for the highest price and
                       lowest price
  --top=TOP            Top K coins ranked in the homepage