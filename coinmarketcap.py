__author__ = 'Li Hao'
# -*- coding:utf-8 -*-

import urllib
import urllib.request
import re
import os
from optparse import OptionParser
import pandas as pd
from HTMLTableParser import  HTMLTableParser


class CoinMarketCapSpider:


    def __init__(self):
        self.homepage = 'https://coinmarketcap.com'
        self.allCoinURL = self.homepage + '/all/views/all/'
        #self.tool = tool.Tool()

    def getPage(self, url, encoding='utf-8'):
        request = urllib.request.Request(url)
        response = urllib.request.urlopen(request)
        return response.read().decode(encoding)


    def getAllCoinPage(self):
        url = self.allCoinURL
        return self.getPage(url)


    def getAllCoinList(self):
        page = self.getAllCoinPage()
        #print('Fetched the page ' + self.allCoinURL)
        pattern = re.compile(
            '<tbody>(.*?)</tbody>',
            re.S
        )
        tbody = re.findall(pattern, page)[0]

        pattern = re.compile(
            '<tr id="id-(.*?)" (.*?)</tr>',
            re.S
        )
        items = re.findall(pattern, tbody)
        coinList = []
        for item in items:

            coin_id = item[0]
            coin_content = item[1].strip()

            pattern = re.compile(
                '<td class="text-center">(.*?)</td>.*?<td class="no-wrap currency-name">.*?<span class="currency-symbol"><a href="(.*?)">(.*?)</a></span>.*?</td>',
                re.S
            )
            attrib = re.findall(pattern, coin_content)

            coin = [coin_id]
            for item in attrib:
                item = [col.strip() for col in item]
                coin += item
            #coin = coin + attrib
            coinList.append(coin)

        # for item in coinList[0]:
        #     print('=',item)
        #     print()

        return coinList

    def getCurrency(self, currency):
        coin, idx, link, shortname = currency
        pageURL = self.homepage + link
        page = self.getPage(pageURL)
        pattern = re.compile(
            '<tbody>(.*?)</tbody>',
            re.S
        )
        tbody = re.findall(pattern, page)[0]

        #print('tbody:', tbody)
        pattern = re.compile(
            '<tr .*?<td>(.*?)</td>.*?<td><a.*?>(.*?)</a></td>.*?<td><a href.*?target="_blank">(.*?)</a></td>.*?<td.*?<span class="volume".*?>(.*?)</span>.*?<td.*?<span.*?>(.*?)</span>.*?</td>.*?<td.*?>(.*?)</td>.*?<td.*?>(.*?)</td>.*?</tr>',
            re.S
        )
        attrib = re.findall(pattern, tbody)

        coin_markets = []
        for coin in attrib:
            coin_market = [item.strip() for item in coin]
            coin_market[3] = float(coin_market[3][1:].replace(',', ''))
            coin_market[4] = float(coin_market[4][1:].replace(',', ''))
            coin_markets.append(coin_market)

        #print(coin_markets)
        return coin_markets



def coinMarkets2Str(coin_markets):
    output = ''
    for coin_market in coin_markets:
        tmp = [str(item) for item in coin_market]
        output += '\t' + '\t'.join(tmp) + '\n'
    return output





def html_table(lol):
  output = ''
  output += '<table>' + '\n'
  for sublist in lol:
      output += '  <tr><td>' + '\n'
      output +=  '    </td><td width=100>'.join([str(item) for item in sublist])  + '\n'
      output += '  </td></tr>' + '\n'
  output +=  '</table>' + '\n'
  return output

def html_table_header():
 return html_table([[]])

def coinMarkets2HTML(coin_markets):
    return html_table(coin_markets)


def printCoinMarket(coin_markets):
    for coin_market in coin_markets:
        tmp = [str(item) for item in coin_market]
        print('\t','\t'.join(tmp))




usage = "coinmarketcap.py --help"
parser = OptionParser(usage=usage)
parser.add_option("--volume", type="int", help="Volume Above", default=100000, dest="volume")
parser.add_option("--numcandi", type="int", help="Number of Candidate coins for the highest price and lowest price", default=3, dest="numcandi")
parser.add_option("--top", type="int", help="Top K coins ranked in the homepage", default=10, dest="top")
parser.add_option("--timer", type="int", help="Timer", default=60, dest="timer")
parser.add_option("--htmltimer", type="int", help="htmltimer", default=10, dest="htmltimer")
parser.add_option("--outputhtml", type="string", help="outputhtml", default='/var/www/html/index.html', dest="outputhtml")
parser.add_option("--diffratio", type="int", help="diffratio", default=30, dest="diffratio")
(options, args) = parser.parse_args()

def diffRatio(top, btm):
    if abs(btm) < 0.000001:
        return 0
    else:
        return (top - btm + 0.0) / btm

def crawl():
    spider = CoinMarketCapSpider()
    contentList = spider.getAllCoinList()

    contentListIdx = 0

    fout = open(options.outputhtml, 'w', encoding='utf-8')
    fout.write('<html><head>\n')
    fout.write('<meta http-equiv="refresh" content="' + str(options.htmltimer) + '">\n')
    fout.write("<link rel='stylesheet' type='text/css' href='overlap.css' />")
    fout.write('</head><body>\n')
    fout.write('<?php \n date_default_timezone_set("Asia/Singapore"); echo date("Y-m-d H:i:s"); echo "(Singapore)" \n ?> ')
    fout.write('<br>\n')
    fout.write(str(options) + '\n')
    fout.write('<br>\n')
    fout.write('<p></p>\n')

    sort_field_name = 'Ratio%'

    for currency in contentList:

        contentListIdx += 1

        coin, idx, link, shortname = currency
        url = spider.homepage + link

        hp = HTMLTableParser()
        table = hp.parse_url(url)[0][1]



        table = table.loc[table['Volume (24h)'] > options.volume]

        maxPrice = table['Price'].max()
        minPrice = table['Price'].min()
        table['minPrice'] = minPrice
        table['maxPrice'] = maxPrice
        table[sort_field_name] = (table['Price'] - minPrice) / minPrice * 100


        table = table.sort_values(by=[sort_field_name], ascending=False)

        table_top = table[:options.numcandi]
        table_btm = table[-options.numcandi:]

        priceIdx = table.columns.get_loc('Price')

        divclass = ''

        topPrice = table.iat[0, priceIdx]
        btmPrice = table.iat[-1, priceIdx]



        diff_ratio = diffRatio(topPrice, btmPrice) * 100

        if diff_ratio > options.diffratio:
            divclass = 'entity_positive'



        fout.write('<div class="' + divclass + '">' + coin + ' (' + "{0:.2f}%".format(diff_ratio) + ', Top:' + str(topPrice) + ', Btm:' + str(btmPrice) + ' )' + '</div>')
        fout.write(table_top.to_html())
        fout.write(table_btm.to_html())
        fout.write('<p></p>')
        fout.write('<p></p>')
        fout.write('<br>')
        fout.flush()



        if options.top == -1:
            continue

        if contentListIdx > options.top:
            break

    fout.write('</body></html>\n')
    fout.close()

def getdatetime(timedateformat='complete'):
    from datetime import datetime
    timedateformat = timedateformat.lower()
    if timedateformat == 'day':
        return ((str(datetime.now())).split(' ')[0]).split('-')[2]
    elif timedateformat == 'month':
        return ((str(datetime.now())).split(' ')[0]).split('-')[1]
    elif timedateformat == 'year':
        return ((str(datetime.now())).split(' ')[0]).split('-')[0]
    elif timedateformat == 'hour':
        return (((str(datetime.now())).split(' ')[1]).split('.')[0]).split(':')[0]
    elif timedateformat == 'minute':
        return (((str(datetime.now())).split(' ')[1]).split('.')[0]).split(':')[1]
    elif timedateformat == 'second':
        return (((str(datetime.now())).split(' ')[1]).split('.')[0]).split(':')[2]
    elif timedateformat == 'millisecond':
        return (str(datetime.now())).split('.')[1]
    elif timedateformat == 'yearmonthday':
        return (str(datetime.now())).split(' ')[0]
    elif timedateformat == 'daymonthyear':
        return ((str(datetime.now())).split(' ')[0]).split('-')[2] + '-' + ((str(datetime.now())).split(' ')[0]).split('-')[1] + '-' + ((str(datetime.now())).split(' ')[0]).split('-')[0]
    elif timedateformat == 'hourminutesecond':
        return ((str(datetime.now())).split(' ')[1]).split('.')[0]
    elif timedateformat == 'secondminutehour':
        return (((str(datetime.now())).split(' ')[1]).split('.')[0]).split(':')[2] + ':' + (((str(datetime.now())).split(' ')[1]).split('.')[0]).split(':')[1] + ':' + (((str(datetime.now())).split(' ')[1]).split('.')[0]).split(':')[0]
    elif timedateformat == 'complete':
        return str(datetime.now())
    elif timedateformat == 'datetime':
        return (str(datetime.now())).split('.')[0]
    elif timedateformat == 'timedate':
        return ((str(datetime.now())).split('.')[0]).split(' ')[1] + ' ' + ((str(datetime.now())).split('.')[0]).split(' ')[0]


def update():
    import time
    while True:
        print('Refreshing... ' + getdatetime())
        crawl()
        time.sleep(options.timer)

update()

# for currency in contentList:
#
#     coin_markets = spider.getCurrency(currency)
#
#
#     sort_coin_markets = sorted(coin_markets,key=lambda tuple:tuple[4], reverse=True)
#
#     sort_coin_markets = [coin_market for coin_market in sort_coin_markets if coin_market[3] > options.volume]
#
#     #print('#total:', len(sort_coin_markets))
#     print(currency[0])
#
#     printCoinMarket(sort_coin_markets[:options.numcandi])
#     print()
#     printCoinMarket(sort_coin_markets[-options.numcandi:])
#     print()
#     print()
#
#     fout.write('<p>' +currency[0] + '</p>\n')
#     fout.write(coinMarkets2HTML(sort_coin_markets[:options.numcandi]) + '\n')
#     fout.write('<br>\n')
#     fout.write(coinMarkets2HTML(sort_coin_markets[-options.numcandi:]) + '\n')
#     fout.write('<br>\n')
#
#     contentListIdx += 1
#     if contentListIdx > options.top:
#         break